package com.tddexerciseapi.tddexerciseapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TddexerciseapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TddexerciseapiApplication.class, args);
	}

}
